import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addExample, clearExamples } from '@redux/slices/appStore';
import ExampleComponent from '@components/ExampleComponent';
import { sendMsg } from '@utils/chrome';

const ExampleContainer = () => {
  const dispatch = useDispatch();
  const { examples } = useSelector((state) => state.appStore);

  const sendExampleMessage = () => {
    sendMsg({
      type: 'example',
      data: 'Hello from App',
      onSuccess: (resp) => dispatch(addExample(resp)),
      onError: () => dispatch(addExample('ERROR - BLAH BLAH BLAH')),
    });
  };

  const clearList = () => {
    dispatch(clearExamples());
  };

  return (
    <ExampleComponent
      examples={examples}
      onClear={clearList}
      onSend={sendExampleMessage}
    />
  );
};

export default ExampleContainer;
