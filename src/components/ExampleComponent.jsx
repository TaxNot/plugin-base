import React from 'react';
import { Button, Typography } from 'antd';

const { Title } = Typography;

const ExampleComponent = ({ onClear, onSend, examples }) => (
  <div>
    <Title>Example Component</Title>
    <Button onClick={onSend}>Send Example Message</Button>
    <Button
      type="dashed"
      danger
      onClick={onClear}
    >
      Clear
    </Button>
    {examples.map((example, i) => (
      <div key={i}>{example}</div>
    ))}
  </div>
);

export default ExampleComponent;
