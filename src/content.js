import $ from 'jquery';

import ContentMananger from './classes/ContentMananger';

window.jQuery = $;
window.$ = $;

chrome.storage.local.get(null, (local) => {
  (() => new ContentMananger(local.content))();
});
