import { createSlice } from '@reduxjs/toolkit';

export const appSlice = createSlice({
  name: 'appStore',
  initialState: {
    examples: [],
  },
  reducers: {
    addExample: (state, action) => {
      state.examples = [
        ...state.examples,
        action.payload,
      ];
    },
    clearExamples: (state) => {
      state.examples = [];
    },
  },
});

export const { addExample, clearExamples } = appSlice.actions;

export default appSlice.reducer;
