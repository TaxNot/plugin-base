import { configureStore } from '@reduxjs/toolkit';
import appStoreReducer from './slices/appStore';

export default (initialState) => {
  const store = configureStore({
    reducer: {
      appStore: appStoreReducer,
    },
    preloadedState: initialState,
  });

  return store;
};
