import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import ExampleContainer from './containers/ExampleContainer';

import 'antd/dist/antd.css';
import './scss/app.scss';

const App = () => {
  const storeState = useSelector((s) => s);
  useEffect(() => {
    // Update local storage on every state change
    chrome.storage.local.set({
      ...storeState,
    });
  });

  return (
    <ExampleContainer />
  );
};

export default App;
