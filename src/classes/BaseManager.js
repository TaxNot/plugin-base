class BaseManager {
  constructor(initialState = {}) {
    this.state = initialState;
  }

  setState = (newState) => {
    this.state = {
      ...this.state,
      ...newState,
    };

    this.bubbleLocal();
  }

  bubbleLocal = () => {
    chrome.storage.local.set({
      content: this.state,
    });
  }
}

export default BaseManager;
