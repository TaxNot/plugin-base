import { addListener } from '@utils/chrome';
import BaseManager from './BaseManager';

class ContentMananger extends BaseManager {
  constructor(initialState = {}) {
    super(initialState);

    addListener(this.msgListener);
  }

  msgListener = (message, sender, response) => {
    const { type, data } = message;

    switch (type) {
      case 'example':
        console.log(`[content.js]. ${data}`);
        response('Hello from Content.js');
        break;
      default:
        console.error('Unknown Message Type');
        response('Unknown Message Type');
        break;
    }
  }
}

export default ContentMananger;
