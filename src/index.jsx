import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import createStore from '@redux/store';

import App from './App';

chrome.storage.local.get(null, (initValues) => {
  const store = createStore(initValues);

  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('app'),
  );
});
