const sendMsg = ({
  type, data = {}, onSuccess = () => {}, onError = () => {},
}) => {
  try {
    chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
      chrome.tabs.sendMessage(tabs[0].id, { type, data }, (resp) => {
        if (!chrome.runtime.lastError) {
          onSuccess(resp);
        } else {
          onError();
        }
      });
    });
  } catch (e) {
    onError(e);
  }
};

const addListener = (cb) => {
  chrome.runtime.onMessage.addListener(cb);
};

export { sendMsg, addListener };
